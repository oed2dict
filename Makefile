PREFIX = /usr

NAME = 'Oxford English Dictionary, 2nd Edition'

.SUFFIXES: .htm .jargon .sed .dict .dz

SRC = 1.htm 2.htm 3.htm 4.htm 5.htm \
      6.htm 7.htm 8.htm 9.htm 10.htm
JARGONS = $(SRC:.htm=.jargon)

all: oed.jargon oed.dict.dz oed.slob stardict/oed.ifo

$(JARGONS): htmtojargon.awk removeetags.pl symbols.sed \
            xmlcleanup.sed xmlcleanup2.sed

.htm.jargon:
	iconv -f ISO-8859-1 -t UTF-8 < "$<" \
	| sed 's/\r//g' \
	| awk -f htmtojargon.awk \
	| sed -f symbols.sed \
	| perl -p removeetags.pl \
	| sed -f xmlcleanup.sed \
	| sed -f xmlcleanup2.sed \
	> "$@"

symbols.sed: symbols symbolstosed.awk
	awk -f symbolstosed.awk < symbols > $@

oed.jargon: $(JARGONS)
	cat $(JARGONS) > $@

oed.dict: oed.jargon
	cat oed.jargon \
	| dictfmt -j --utf8 \
	  --columns 0 --allchars \
	  --headword-separator ',' \
	  --index-data-separator "	" \
	  -u http://njw.name/oed2dict \
	  -s "$(NAME)" \
	  oed

oed.dict.dz: oed.dict
	dictzip -k oed.dict

oed.slob: oed.dict.dz
	pyglossary oed.index $@
	slob tag -n label -v "$(NAME)" oed.slob

stardict/oed.ifo: oed.slob
	mkdir -p stardict
	pyglossary oed.slob stardict/$@

install: all
	chmod 644 oed.dict.dz oed.index
	cp oed.dict.dz oed.index $(DESTDIR)$(PREFIX)/share/dictd/
	test -x /usr/sbin/dictdconfig && dictdconfig -w
	test -x /etc/init.d/dictd && /etc/init.d/dictd restart

.PHONY: all install
