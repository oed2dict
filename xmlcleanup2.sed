#!/bin/sed -f
# sed rules that must run after the main set

# any xml tags not processed can just go away
s/<[^>]*>//g
# any xml character entities not processed can just go away
s/&[^;]*;//g
