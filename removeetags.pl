# <e> tags seem to duplicate other preceeding tags, so remove them
#
# sed doesn't support non-greedy matching, so we're using perl
s/<e>.*?<\/e>//g
