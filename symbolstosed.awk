#!/usr/bin/awk -f

/^[^#]/{
	printf("s/_%s_/%s/g\n", $1, $2);
	printf("s/&%s;/%s/g\n", $1, $2);
}
